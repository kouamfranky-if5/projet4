// import { recipes } from "../assets/data/recipes"; 
import { createGeneralSearch, filterRecette, onclickChevron } from './filter.js';
import { getInfosDataRecette, initCorpsRecettes, onclickReadMoreDescriptionRecette } from './recettes.js';
import { sortDataRecetteByName } from './utils.js';
// recuperation des donnees des recettes
export const dataRecettes = sortDataRecetteByName(recipes);
// console.log(dataRecettes);

// let dataIngredients = [];
// let dataUstensiles = [];
// let dataAppareils = [];

// getInfosDataRecette(dataRecettes,dataAppareils,dataUstensiles,dataIngredients);
 

// Construction de la page d'accueil
const init = function () {
    // initialisation du filtre
    createGeneralSearch(dataRecettes);
    // initialisation des recettes
    initCorpsRecettes(dataRecettes);
    // initialisation de l'evenement de click sur la description des recettes 
    onclickReadMoreDescriptionRecette(dataRecettes);
    // ecoute du filtre generale
    filterRecette(dataRecettes);
     
};

init();




