import { accepGeneralSearch, lenghtMinInputSearh } from './config-system.js';

export function getDataTagsSelected(html, idTagSelect) {
    return Array.from(html.querySelector(idTagSelect).children).map(item => item.firstChild.data)
}

export function filterMethode1(dataRecette, token, ingredients = [], ustensils = [], appareils = []) {
    let dataFilterByToken = (token.length > lenghtMinInputSearh || accepGeneralSearch) ? dataRecette.filter(item =>
        item.name.toLowerCase().includes(token.toLowerCase())
        || item.description.toLowerCase().includes(token.toLowerCase())
        || chaineInArray(item.ingredients.map(elt => elt.ingredient.toLowerCase()), token)
    ) : dataRecette;
    let responses = (ingredients.length > 0 || ustensils.length > 0 || appareils.length > 0) ?
        dataFilterByToken.filter(item => (
            (arrayInOtherArrayPatial(item.ingredients.map(e => e.ingredient), ingredients)
                && (arrayInOtherArrayPatial(item.ustensils, ustensils)))
            && chaineInArray(appareils, item.appliance)
        )) : dataFilterByToken;

    return responses;
}

export function filterNatif(dataRecette, token, ingredients = [], ustensils = [], appareils = []) {
    let dataRFilter = [];
    for (let index = 0; index < dataRecette.length; index++) {
        const item = dataRecette[index];
        if (item.name.toLowerCase().includes(token.toLowerCase())
            || item.description.toLowerCase().includes(token.toLowerCase())
            || someArraysNatif(item.ingredients.map(elt => elt.ingredient), token)
        ) {
            dataRFilter.push(item);
        }
    }
    return dataRFilter;
}

function someArraysNatif(arrays, token) {
    for (let i = 0; i < arrays.length; i++) {
        if (arrays[i] && arrays[i].toLowerCase().includes(token.toLowerCase()))
            return true;
    }
}

function includesArraysNatif(arrays, token) {
    for (let i = 0; i < arrays.length; i++) {
        if (arrays[i] == token)
            return true;
    }
}

function includesChaineNatif(chaine, token) {
    for (let i = 0; i < chaine.length; i++) {
        if (chaine[i] == token) {

        }
    }
}

// verifier si un tableau est contenu dans un autre
export function arrayInOtherArrayAbsolu(arrayPrincipal = [], arrayDeTest = []) {
    if (arrayDeTest.length <= 0) return true
    // console.log(arrayPrincipal,arrayDeTest);
    let allFounded = arrayDeTest.every(ai => arrayPrincipal.includes(ai));
    // let isFounded = arr1.some(ai => arr2.includes(ai)); 
    // console.log(allFounded);
    return allFounded;
}
export function arrayInOtherArrayPatial(arrayPrincipal = [], arrayDeTest = []) {
    if (arrayDeTest.length <= 0) return true
    // console.log(arrayPrincipal,arrayDeTest);
    let allFounded = arrayPrincipal.some(ai => arrayDeTest.includes(ai)); 
    // let isFounded = arrayPrincipal.some(ai => arrayDeTest.includes(ai)); 
    // console.log(allFounded);
    return allFounded;
}
export function chaineInArray(array = [], token = ``) {
    if (array.length <= 0) return true;
    return array.some(i => i.toLowerCase().includes(token.toLowerCase()));
}

export function sortDataRecetteByName(data) {
    return data.sort((item1, item2) => (item1.name > item2.name) ? 1 : (item2.name > item1.name) ? -1 : 0);
}