import { lenghtMinInputSearh, methode } from './config-system.js';
import { dataRecettes } from './index.js';
import { getInfosDataRecette, initCorpsRecettes, onclickReadMoreDescriptionRecette } from './recettes.js';
import { filterMethode1, filterNatif, getDataTagsSelected } from './utils.js';

let tokenSearch = '';
// let dataRecettesFilter;

export function createGeneralSearch(dataInfoRecettes) {
    let dataAppareils = [];
    let dataUstensiles = [];
    let dataIngredients = [];
    getInfosDataRecette(dataInfoRecettes, dataAppareils, dataUstensiles, dataIngredients);
    let htmlSearchTagRecette = document.querySelector("#search_by_tags_recette");
    htmlSearchTagRecette.innerHTML = `
        <div class="input-search">
            <input id="search_text" class="form-control me-2" type="search" placeholder="Rechercher une recette" aria-label="Search">
            <i id="btn-search" class="fa fa-search"> </i>
        </div>
        <div class="bd-highlight mt-2 float-end"> Total : <span id="total_recettes" > ${dataInfoRecettes.length} </span> </div>
        <div class="d-sm-inline d-md-flex mt-4" >
            <div class="mw-33 mw-sm-100" > 
                <div id="tags_select_ingredient" class="d-flex flex-wrap" > </div>
            </div>
            <div class="mw-33 mw-sm-100" > 
                <div id="tags_select_appareil" class="d-flex flex-wrap"> </div>
            </div>
            <div class="mw-33 mw-sm-100" > 
                <div id="tags_select_ustensil" class="d-flex flex-wrap"> </div>
            </div>
        </div>
        <div class="d-md-flex mt-2 d-sm-inline w-100 align-items-start text-white position-absolute z-n2"> 
            <div class="mb-3 mw-33 mw-sm-100" >
                <nav id="filter_ingredient" class="rounded bg-primary p-3 me-md-2 me-sm-0">
                    <label class="d-flex justify-content-between align-items-center fw-bold cursor-pointer">
                        <span class="text_filter">Ingredients </span>
                        <span > <i class="ms-2 fas fa-chevron-down"></i> </span>
                    </label>
                    <div class="d-none">
                        <div class="d-flex flex-row justify-content-between align-items-center">
                            <input id="search_ingrediant" class="w-100 p-0 pb-1 pt-0 form-control border-0 bg-transparent text-white"
                                type="search" placeholder="Rechercher un ingredient" aria-label="Search">
                                <span id="ingrediant-chevron-up"> <i class="fas fa-chevron-up"></i></span>
                        </div>
                        <div id="body_tags--ingredient" class="d-flex flex-row flex-wrap body_tags ">
                        </div>
                    </div>
                </nav>
            </div>
            <div class="mb-3 mx-md-2 mx-sm-0 mw-33 mw-sm-100" > 
                <nav id="filter_appareil" class="rounded bg-success p-3 me-md-2 me-sm-0">
                    <label class="d-flex justify-content-between align-items-center fw-bold cursor-pointer">
                        <span class="text_filter">Appareils </span>
                        <span> <i class="ms-2 fas fa-chevron-down"></i> </span>
                    </label>
                    <div class="d-none">
                        <div class="d-flex justify-content-between align-items-center">
                            <input id="search_appareils" class="w-100 p-0 pb-1 pt-0 form-control border-0 bg-transparent text-white"
                                type="search" placeholder="Rechercher un appareil" aria-label="Search">
                                <span id="appareil-chevron-up"> <i class="fas fa-chevron-up"></i></span> 
                        </div>
                        <div id="body_tags--appareil" class="d-flex flex-row flex-wrap  body_tags">
                            
                        </div>
                    </div>
                </nav>
            </div>
            
            <div class="mb-3 mw-33 mw-sm-100" >  
                <nav id="filter_ustensile" class="rounded bg-danger p-3 me-md-0 me-sm-0">
                    <label class="d-flex justify-content-between align-items-center fw-bold cursor-pointer">
                        <span class="text_filter">Ustensiles </span>
                        <span> <i class="ms-2 fas fa-chevron-down"></i> </span> 
                    </label>
                    <div class="d-none">
                        <div class="d-flex justify-content-between align-items-center">
                            <input id="search_ustensil" class="w-100 p-0 pb-1 pt-0 form-control border-0 bg-transparent text-white"
                                type="search" placeholder="Rechercher un ustensil" aria-label="Search">
                                <span id="ustensile-chevron-up"> <i class="fas fa-chevron-up"></i></span> 
                        </div>
                        <div id="body_tags--ustensil" class="d-flex flex-row flex-wrap  body_tags">
                            
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    `;
    // ajout de l'evenement de click un tag
    getTags(dataIngredients, `#body_tags--ingredient`, "#tags_select_ingredient", "bg-primary");
    getTags(dataUstensiles, `#body_tags--ustensil`, "#tags_select_ustensil", "bg-danger");
    getTags(dataAppareils, `#body_tags--appareil`, "#tags_select_appareil", "bg-success");
    // ecoute des sous filtre sur le listing des tags pour chaque rubrique
    htmlSearchTagRecette.querySelector("#search_ingrediant").addEventListener('keyup', (event) => filterTags(event.target.value, dataIngredients, `#body_tags--ingredient`, "#tags_select_ingredient", "bg-primary"));
    htmlSearchTagRecette.querySelector("#search_ustensil").addEventListener('keyup', (event) => filterTags(event.target.value, dataUstensiles, `#body_tags--ustensil`, "#tags_select_ustensil", "bg-danger"));
    htmlSearchTagRecette.querySelector("#search_appareils").addEventListener('keyup', (event) => filterTags(event.target.value, dataAppareils, `#body_tags--appareil`, "#tags_select_appareil", "bg-success"));
    htmlSearchTagRecette.querySelector("#btn-search").addEventListener('click', () => filterGeneral(tokenSearch, dataRecettes, getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ingredient"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ustensil"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_appareil")));

    // ecouter les evement de clic sur les chevron Up et Down
    onclickChevron();
}

export function onclickChevron() {
    let ingredient = document.getElementById("filter_ingredient");
    let appareil = document.getElementById("filter_appareil");
    let ustensile = document.getElementById("filter_ustensile");
    // console.log(ingredient);
    ingredient.firstElementChild.addEventListener("click", (event) => filterTagsDown(event, ingredient));
    appareil.firstElementChild.addEventListener("click", (event) => filterTagsDown(event, appareil));
    ustensile.firstElementChild.addEventListener("click", (event) => filterTagsDown(event, ustensile));

    function filterTagsDown(event, tag) {
        event.preventDefault();
        tag.firstElementChild.classList.remove("d-flex");
        tag.firstElementChild.classList.add("d-none");
        tag.lastElementChild.classList.remove("d-none");
        tag.lastElementChild.classList.add("d-inline");
        return tag;
    }

    let ingredientUp = document.getElementById("ingrediant-chevron-up");
    let appareilUp = document.getElementById("appareil-chevron-up");
    let ustensileUp = document.getElementById("ustensile-chevron-up");
    ingredientUp.addEventListener("click", (event) => filterTagsUp(event, ingredient));
    appareilUp.addEventListener("click", (event) => filterTagsUp(event, appareil));
    ustensileUp.addEventListener("click", (event) => filterTagsUp(event, ustensile));
    function filterTagsUp(event, tag) {
        tag.firstElementChild.classList.remove("d-none");
        tag.firstElementChild.classList.add("d-flex");
        tag.lastElementChild.classList.remove("d-inline");
        tag.lastElementChild.classList.add("d-none");
    }
}
// permet de filtrer les tags pour chaque rubrique de filtre (Ingrediant, Ustensils, Appareils)
function filterTags(token, tags, tagIdNode, idTagSelect, bgColorSelect) {
    // filtre des tags
    let tagsFilter = tags.filter(item => item.toLowerCase().includes(token.toLowerCase()));
    // envoie des elements filtre pour la mise a jours des evements avec affichage au niveau du DOM
    getTags(tagsFilter, tagIdNode, idTagSelect, bgColorSelect);
}
// permet de recuperer l'ensemble des tags d'une rubrique (tout les evenement necessaire y son construit)
function getTags(tags, tagIdNode, idTagSelect, bgColorSelect) {

    let htmlSearchTagRecette = document.getElementById("search_by_tags_recette");
    let node = htmlSearchTagRecette.querySelector(tagIdNode);
    while (node.firstChild) {
        node.removeChild(node.firstChild)
    }
    tags.forEach(item => {
        let tag = document.createElement("p");
        tag.classList.add("tag_ingredient", "bd-highlight");
        tag.innerText = item;
        tag.addEventListener("click", (event) => onclickTag(event, idTagSelect));
        function onclickTag(event, idTagSelect) {
            // recuperation du parent des noeuds selectionnees
            let nodeSelecteTags = htmlSearchTagRecette.querySelector(idTagSelect);

            // construction des tags selectionnees
            let tagSelect = document.createElement("p");
            tagSelect.outerText
            tagSelect.classList.add("badge", bgColorSelect, "me-2", "px-2", "py-2", "d-flex", "align-items-center");
            tagSelect.innerText = event.target.innerText;
            // creation du bouton de fermeture du tag selectionne
            let close = document.createElement("span");
            close.classList.add("cursor-pointer", "p-1", "pt-0", "ms-2", "border", "rounded-circle");
            close.innerText = `x`;
            tagSelect.appendChild(close);
            nodeSelecteTags.appendChild(tagSelect);

            for (let index = 0; index < nodeSelecteTags.children.length; index++) {
                const item = nodeSelecteTags.children.item(index);
                if (item && item.firstChild.data == event.target.innerText && item != tagSelect)
                    nodeSelecteTags.removeChild(tagSelect);
            }
            // evenement de click pour la supression d'un tag de filtre
            close.addEventListener("click", (event) => onclickCloseTag(event, nodeSelecteTags, tagSelect));
            function onclickCloseTag(event, nodeSelecteTags, tagSelect) {
                nodeSelecteTags.removeChild(tagSelect);
                filterGeneral(tokenSearch, dataRecettes, getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ingredient"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ustensil"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_appareil"));
            }

            // console.log("#tags_select_ingredient", Array.from(html.querySelector("#tags_select_ingredient").children).map(item => item.firstChild.data));
            // console.log("#tags_select_ustensil", Array.from(html.querySelector("#tags_select_ustensil").children).map(item => item.firstChild.data));
            // console.log("#tags_select_appareil", Array.from(html.querySelector("#tags_select_appareil").children).map(item => item.firstChild.data));
            filterGeneral(tokenSearch, dataRecettes, getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ingredient"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ustensil"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_appareil"));
        }
        node.appendChild(tag);
    });
}
// filtre generale des recettes
export function filterRecette(dataRecettes) {
    let htmlSearchTagRecette = document.getElementById("search_by_tags_recette");
    let searchText = document.getElementById("search_text");
    searchText.addEventListener("keyup", (event) => {
        tokenSearch = event.target.value;
        // console.log(tokenSearch);

        if (event.key === "Enter")
            filterGeneral(tokenSearch, dataRecettes, getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ingredient"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ustensil"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_appareil"));

        if (tokenSearch.length >= lenghtMinInputSearh)
            filterGeneral(tokenSearch, dataRecettes, getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ingredient"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_ustensil"), getDataTagsSelected(htmlSearchTagRecette, "#tags_select_appareil"));
    });
}



function filterGeneral(token, dataRecettes, ingredients = [], ustensils = [], appareils = []) {
    // premiere methode
    let dataRecettesFilter = (methode == 1) ? filterMethode1(dataRecettes, token, ingredients, ustensils, appareils)
        : filterNatif(dataRecettes, token, ingredients, ustensils, appareils);

    let totalRecettes = document.getElementById("total_recettes");
    // console.log(totalRecettes); totalRecettes.innerText=totalRecettes.length;
    if (totalRecettes) totalRecettes.innerText =dataRecettesFilter.length;
    // console.log(dataRecettesFilter);
    // ajout de l'evenement de click un tag
    let dataAppareils = [];
    let dataUstensiles = [];
    let dataIngredients = [];
    getInfosDataRecette(dataRecettesFilter, dataAppareils, dataUstensiles, dataIngredients);
    getTags(dataIngredients, `#body_tags--ingredient`, "#tags_select_ingredient", "bg-primary");
    getTags(dataUstensiles, `#body_tags--ustensil`, "#tags_select_ustensil", "bg-danger");
    getTags(dataAppareils, `#body_tags--appareil`, "#tags_select_appareil", "bg-success");

    initCorpsRecettes(dataRecettesFilter);
    onclickReadMoreDescriptionRecette(dataRecettesFilter);
}