import { nbreCaractereDescriptionRecette } from './config-system.js';

export function initCorpsRecettes(recettes) {
    let html = document.querySelector("#recettes");
    let content = ``;
    if (recettes.length==0) {
        content = `<div class="text-center mt-5 fs-3 bd-highlight">
                    <p>Aucune recette correspondante à la recherche <img src="assets/images/16334775943974_image36.gif" alt=""></p>
                    
                </div> `;
    } else
        recettes.forEach(recette => {
            let btnReadMore = document.createElement("span");
            btnReadMore.classList.add("read", "fst-italic", "fw-bold");
            btnReadMore.setAttribute("id", `readmore_${recette.id}`);
            btnReadMore.innerText = `read more`;

            let moreText = document.createElement("span");
            moreText.setAttribute('id', `more_${recette.id}`);
            moreText.classList.add('hidden_text');
            moreText.innerText = recette.description.substr(nbreCaractereDescriptionRecette);

            let dots = document.createElement("span");
            dots.setAttribute('id', `dots_${recette.id}`);
            dots.classList.add('show_text');
            dots.innerText = `...`;

            content += `
            <div class="card col-sm-6  col-md-4  py-3  border-0">
                <div class="card-header border-0"> </div>
                <div class="card-body">
                    <div class="d-flex justify-content-between align-self-start">
                        <h5 class="card-title fw-normal">${recette.name}</h5>
                        <p class="card-title fw-bold ms-2 pt-1 flex-shrink-0"> <i class="far fa-clock"></i> ${recette.time} min</p>
                    </div>
                    <div class="row">
                        <div class="pe-2 col-5 bd-highlight">
                            ${getIngrediantRecette(recette.ingredients)}
                        </div>
                        <p class="ps-4 mb-0 col-7 bd-highlight ">
                            ${recette.description.length > nbreCaractereDescriptionRecette ?
                    `${recette.description.substr(0, nbreCaractereDescriptionRecette)} ${dots.outerHTML} ${moreText.outerHTML} ${btnReadMore.outerHTML}` : `${recette.description}`}
                        </p>
                        
                    </div>
                </div>
            </div>
        `;


        });

    html.innerHTML = content;
}

function getIngrediantRecette(ingredients) {
    let content = ``;
    ingredients.forEach(item => {
        content += `<p class="p-0 m-0"> <span class="fw-bold">${item.ingredient}:</span> ${item.quantity ? item.quantity : '-'} </p>`;
    });
    return content;
}

export function onclickReadMoreDescriptionRecette(recettes) {
    recettes.forEach(recette => {
        let btnReadMore = document.getElementById("readmore_" + recette.id);
        let dots = document.getElementById("dots_" + recette.id);
        let moreText = document.getElementById("more_" + recette.id);
        if (recette.description.length > nbreCaractereDescriptionRecette)
            btnReadMore.addEventListener("click", function (event) {
                if (dots.classList.contains("hidden_text")) {
                    dots.classList.remove("hidden_text");
                    dots.classList.add("show_text");
                    btnReadMore.innerHTML = "read more";
                    moreText.classList.remove("show_text");
                    moreText.classList.add("hidden_text");
                } else {
                    dots.classList.remove("show_text");
                    dots.classList.add("hidden_text");
                    btnReadMore.innerHTML = "read less";
                    moreText.classList.add("show_text");
                    moreText.classList.remove("hidden_text");
                }
            });
    });
}

export function getInfosDataRecette(dataRecettes, dataAppareils, dataUstensiles, dataIngredients) {
    dataRecettes.forEach(recette => {
        if (!dataAppareils.includes(recette.appliance))
            dataAppareils.push(recette.appliance);
        recette.ustensils.forEach(item => {
            if (!dataUstensiles.includes(item))
                dataUstensiles.push(item);
        });
        recette.ingredients.forEach(item => {
            if (!dataIngredients.includes(item.ingredient))
                dataIngredients.push(item.ingredient);
        });
    });
    dataAppareils.sort();
    dataUstensiles.sort();
    dataIngredients.sort();
}